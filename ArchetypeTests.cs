﻿using Meep.Tech.Data;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Examples;
using Meep.Tech.Data.Static;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Meep.Tech.ECSBAM.Tests {

  [TestClass]
  public class ArchetypeTests : BaseArchetypeDataTest {

    #region Equality Tests

    [TestMethod]
    public void EqualsSelf_Success() {
      Item.Type firstAppleType = Item.Types.Get(Apple.Id);
      Item.Type secondAppleType = Item.Types.Get(Apple.Id);

      Assert.AreEqual(firstAppleType, secondAppleType);
    }

    [TestMethod]
    public void EqualsNull_Failure() {
      Item.Type nullType = null;
      Item.Type appleType = Item.Types.Get(Apple.Id);

      Assert.AreNotEqual(nullType, appleType);
    }

    [TestMethod]
    public void NullEqualsArchetype_Failure() {
      Item.Type appleType = Item.Types.Get(Apple.Id);
      Item.Type nullType = null;

      Assert.AreNotEqual(appleType, nullType);
    }

    [TestMethod]
    public void EqualsOtherOfSameBase_Failure() {
      Item.Type bookType = Item.Types.Get(Book.Id);
      Item.Type appleType = Item.Types.Get(Apple.Id);

      Assert.AreNotEqual(bookType, appleType);
    }

    [TestMethod]
    public void EqualsOtherOfDifferentBase_Failure() {
      Item.Type bookType = Item.Types.Get(Book.Id);
      Tile.Type appleType = Tile.Types.Get(Grass.Id);

      Assert.AreNotEqual(bookType, appleType);
    }

    #endregion

    [TestClass]
    public class MakeModelTests : BaseArchetypeDataTest {

      #region Object Based

      /// <summary>
      /// This test makes sure the created item is of the right type
      /// </summary>
      [TestMethod]
      public void Object_CorrectType() {
        Item item = Item.Types.Get(Apple.Id).Make();

        Assert.AreEqual(item.type, Apple.Id.Archetype);
      }

      /// <summary>
      /// This test makes sure the created item is of the right type
      /// TODO: add an extension model to Item
      /// </summary>
      [TestMethod]
      public void Object_CorrectType_Generic() {
        Item item = Item.Types.Get(Apple.Id).Make<Item>();

        Assert.AreEqual(item.type, Apple.Id.Archetype);
      }

      /// <summary>
      /// This test makes sure the created item is of the right type
      /// </summary>
      [TestMethod]
      public void Object_CorrectType_Static() {
        Item item = Archetype<Apple>.Make<Item>();

        Assert.AreEqual(item.type, Archetype<Apple>.Get());
      }

      /// <summary>
      /// This test makes sure the value passed in via a param is set in the resulting object after finalize is called.
      /// </summary>
      [TestMethod]
      public void Object_SetValueFromParam() {
        string testId = "test";

        Item item = Item.Types.Get(11).Make(new Dictionary<Param, object> {{
            Param.UniqueId, testId
        }});

        Assert.AreEqual(item.getUniqueId(), testId);
      }

      #endregion

      #region Struct Based

      /// <summary>
      /// This test makes sure the created item is of the right type
      /// </summary>
      [TestMethod]
      public void Struct_CorrectType() {
        Tile tile = Tile.Types.Get(Grass.Id).Make();

        Assert.AreEqual(tile.type, Grass.Id.Archetype);
      }

      /// <summary>
      /// This test makes sure the created item is of the right type
      /// </summary>
      [TestMethod]
      public void Struct_CorrectType_Generic() {
        Tile tile = Tile.Types.Get(Grass.Id).Make<Tile>();

        Assert.AreEqual(tile.type, Grass.Id.Archetype);
      }

      /// <summary>
      /// This test makes sure the created item is of the right type
      /// </summary>
      [TestMethod]
      public void Struct_CorrectType_Static() {
        Tile tile = Archetype<Grass>._.Make();

        Assert.AreEqual(tile.type, Archetype<Grass>._);
      }

      /// <summary>
      /// This test makes sure the value passed in via a param is set in the resulting object after finalize is called.
      /// </summary>
      [TestMethod]
      public void Struct_SetValueFromParam() {
        Tile.Type.Color testColor = Tile.Type.Color.Green;

        Tile tile = Tile.Types.Get(Grass.Id).Make(new Dictionary<Param, object> {{
            Tile.Type.Params.Color, testColor
        }});

        Assert.AreEqual(tile.color, testColor);
      }

      #endregion
    }
  }
}
