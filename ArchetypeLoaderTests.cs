﻿using Meep.Tech.Data.Examples;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Loading;
using Meep.Tech.Data.Static;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.ECSBAM.Tests {

  [TestClass]
  public class ArchetypeLoaderTests : BaseArchetypeDataTest {

    #region Archetypes Static Data Loaded Correctly

    /// <summary>
    /// You must add any new test archetypes that are loaded here by id:
    /// </summary>
    public static IEnumerable<IArchetypeId> AllTestAchetypes {
      get;
    } = new IArchetypeId[] {
      Apple.Id,
      Grass.Id,
      Book.Id,
      Sword.Id,
      SpecialSword.Id,
      WeaponStatsLinkSystemComponent.Type.Id,
      WeaponDamageMultiplierModLinkType.Id, 
      WeaponStatsLinkSystemComponent.ArchetypeData.Id,
      WeaponStatsLinkSystemComponent.ModelData.Id,
      Stackable.TypeId,
      StackQuantity.TypeId,
      StackQuantityStackableLink.TypeId,
      WeaponWear.Type.Id,
      ThrowingStars.Id,
      WeaponDamageMultiplierModLinkType.Id
    };

    [TestMethod]
    public void All_CountIsCorrect() {
      Assert.AreEqual(
        AllTestAchetypes.Count(),
        Archetypes.All.Count
      );
    }

    [TestMethod]
    public void All_RegisteredFirstArchetype() {
      Assert.IsTrue(Archetypes.All.ContainsKey(AllTestAchetypes.First().Archetype.Id));
    }

    [TestMethod]
    public void All_RegisteredFirstId() {
      Assert.IsTrue(Archetypes.Ids.ContainsKey(AllTestAchetypes.First().ExternalId));
    }

    [TestMethod]
    public void Ids_CountMatchesArchetypeCount() {
      Assert.AreEqual(
        Archetypes.Ids.Count,
        Archetypes.All.Count
      );
    }

    [TestMethod]
    public void AllCollections_RegisteredFirstId() {
      Assert.IsTrue(Archetypes.Collections.ContainsKey(AllTestAchetypes.First().Archetype.BaseArchetypeType));
    }

    [TestMethod]
    public void SystemType_AsArchetype() {
      Assert.AreEqual(
        Grass.Id.Archetype,
        typeof(Grass).AsArchetype()
      );
    }

    #endregion

    #region Registration Lock and Sealing

    [TestMethod]
    public void Register_FailureAfterFinishing() {
      Assert.ThrowsException<ArchetypeLoader.AttemptedRegistryWhileSealedException>(
        () => Item.Types.ReRegisterToSubCollection(Apple.Id.Archetype)
      );
    }

    #endregion
  }
}
