﻿using Meep.Tech.Data.Examples;
using Meep.Tech.Data.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Meep.Tech.ECSBAM.Tests {

  [TestClass]
  public class ComponentTests : BaseArchetypeDataTest {

    [TestMethod]
    public void SerializeModelDataComponent() {
      StackQuantity original = new StackQuantity(100);

      var serializedData = original.serialize();

      StackQuantity deserialized = (StackQuantity.TypeId.Archetype as StackQuantity.Type).Make();
      deserialized = (StackQuantity)deserialized.deserialize(serializedData);

      Assert.AreEqual(original, deserialized);
    }

    [TestMethod] 
    public void Execute_SimpleLinkSystemLogicFunction_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      Assert.AreEqual(
        11,
        weapon.execute(WeaponStatsLinkSystemComponent.Type.CalculateDamageTotal)
      );
    }

    [TestMethod]
    public void Execute_SimpleLinkSystemLogicFunction_Failure() {
      Item item = Item.Types.Get("Items.Apple").Make();
      // TODO: this should throw a custom descriptive exception
      Assert.ThrowsException<KeyNotFoundException>(
        () => item.execute(WeaponStatsLinkSystemComponent.Type.CalculateDamageTotal)
      );
    }

    [TestMethod] 
    public void TryExecute_SimpleLinkSystemLogicFunction_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.tryToExecute(WeaponStatsLinkSystemComponent.Type.CalculateDamageTotal, out int result);
      Assert.AreEqual(11, result);
    }

    [TestMethod] 
    public void TryExecute_SimpleLinkSystemLogicFunction_Success() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      Assert.IsTrue(weapon.tryToExecute(WeaponStatsLinkSystemComponent.Type.CalculateDamageTotal, out _));
    }

    [TestMethod] 
    public void TryExecute_SimpleLinkSystemLogicFunction_Failure() {
      Item item = Item.Types.Get("Items.Apple").Make();
      Assert.IsFalse(item.tryToExecute(WeaponStatsLinkSystemComponent.Type.CalculateDamageTotal, out _));
    }

    [TestMethod] 
    public void Execute_AdvancedLinkSystemLogicFunction_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      Assert.AreEqual(
        11,
        weapon.execute(WeaponStatsLinkSystemComponent.Type.AdvancedCalculateDamageTotal)
      );
    }

    [TestMethod]
    public void Execute_AdvancedLinkSystemLogicFunction_Failure() {
      Item item = Item.Types.Get("Items.Apple").Make();
      // TODO: this should throw a custom descriptive exception
      Assert.ThrowsException<KeyNotFoundException>(
        () => item.execute(WeaponStatsLinkSystemComponent.Type.AdvancedCalculateDamageTotal)
      );;
    }

    [TestMethod] 
    public void TryExecute_AdvancedLinkSystemLogicFunction_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.tryToExecute(WeaponStatsLinkSystemComponent.Type.AdvancedCalculateDamageTotal, out int result);
      Assert.AreEqual(11, result);
    }

    /// <summary>
    /// TODO: More with input and more modded tests.
    /// </summary>
    [TestMethod] 
    public void TryExecute_AdvancedLinkSystemLogicFunction_WithInput_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.tryToExecute(
        WeaponStatsLinkSystemComponent.Type.TestFunctionWithInput,
        out string result,
        (weapon, 1)
      );
      Assert.AreEqual(1.ToString() + weapon.getUniqueId(), result);
    }

    [TestMethod] 
    public void TryExecute_AdvancedLinkSystemLogicFunction_Success() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      Assert.IsTrue(weapon.tryToExecute(WeaponStatsLinkSystemComponent.Type.AdvancedCalculateDamageTotal, out _));
    }

    [TestMethod] 
    public void TryExecute_AdvancedLinkSystemLogicFunction_Failure() {
      Item item = Item.Types.Get("Items.Apple").Make();
      Assert.IsFalse(item.tryToExecute(WeaponStatsLinkSystemComponent.Type.AdvancedCalculateDamageTotal, out _));
    }

    [TestMethod] 
    public void Execute_SimpleLinkSystemLogicFunction_Null_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.execute(WeaponStatsLinkSystemComponent.Type.IncreaseDamageBonus);
      Assert.AreEqual(
        2, 
        (weapon.getComponent(WeaponStatsLinkSystemComponent.ModelData.Id) as WeaponStatsLinkSystemComponent.ModelData).damageBonus
      );
    }

    [TestMethod]
    public void Execute_SimpleLinkSystemLogicFunction_Null_Failure() {
      Item item = Item.Types.Get("Items.Apple").Make();
      // TODO: this should throw a custom descriptive exception
      Assert.ThrowsException<KeyNotFoundException>(
        () => item.execute(WeaponStatsLinkSystemComponent.Type.IncreaseDamageBonus)
      );
    }

    [TestMethod] 
    public void TryExecute_SimpleLinkSystemLogicFunction_Null_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.tryToExecute(WeaponStatsLinkSystemComponent.Type.IncreaseDamageBonus);
      Assert.AreEqual(
        2,
        (weapon.getComponent(WeaponStatsLinkSystemComponent.ModelData.Id) as WeaponStatsLinkSystemComponent.ModelData).damageBonus
      );
    }

    [TestMethod] 
    public void Execute_TryGetSystem_ModdableLinkSystemLogicFunction_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.tryToGetSystem(out WeaponStatsLinkSystemComponent system);

      // we need to know the base of the system link type we want at the least:
      WeaponStatsLinkSystemComponent.Type systemWhosModdableFunctionWeWant 
        = system.type as WeaponStatsLinkSystemComponent.Type;

      int result = systemWhosModdableFunctionWeWant.ModdableCalculateDamageTotal(
        weapon.getComponent<WeaponStatsLinkSystemComponent.ModelData>(),
        weapon.type.GetData<WeaponStatsLinkSystemComponent.ArchetypeData>()
      );

      Assert.AreEqual(
        11,
        result
      );
    }

    [TestMethod] 
    public void Execute_Modded_TryGetSystem_ModdableLinkSystemLogicFunction_Value() {
      // Make a weapon of type Special Sword, which uses a moded damage calculation in it's config:
      Weapon weapon = Item.Types.Get("Items.Weapons.SpecialSword").Make<Weapon>();

      // Get the modded system attached to the item's type:
      weapon.tryToGetSystem(out WeaponStatsLinkSystemComponent system);

      // we need to know the base of the system link type we want:
      WeaponStatsLinkSystemComponent.Type systemWhosModdableFunctionWeWant 
        = system.type as WeaponStatsLinkSystemComponent.Type;

      // use the system:
      int result 
        = systemWhosModdableFunctionWeWant.ModdableCalculateDamageTotal.ExecuteFor(weapon);

      Assert.AreEqual(
        // this is 1000, not 100 because the example changes it twice.
        1000,
        result
      );
    }

    [TestMethod] 
    public void TryGetSystem_ModdableLinkSystemLogicFunction_Equals() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.tryToGetSystem(out WeaponStatsLinkSystemComponent system);

      Assert.AreEqual(
        system,
        weapon.type.SystemComponents[WeaponStatsLinkSystemComponent.Type.Id]
      );
    }

    [TestMethod] 
    public void TryGetSystem_ModdableLinkSystemLogicFunction_Bool() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();

      Assert.IsTrue(weapon.tryToGetSystem(out WeaponStatsLinkSystemComponent _));
    }

    [TestMethod] 
    public void ExecuteFor_TryGetSystem_ModdableLinkSystemLogicFunction_Null_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.tryToGetSystem(out WeaponStatsLinkSystemComponent system);

      // we need to know the base of the system link type we want at the least:
      WeaponStatsLinkSystemComponent.Type systemWhosModdableFunctionWeWant 
        = system.type as WeaponStatsLinkSystemComponent.Type;

      int result
        = systemWhosModdableFunctionWeWant.ModdableCalculateDamageTotal
          .ExecuteFor(weapon);

      Assert.AreEqual(
        11,
        result
      );
    }

    [TestMethod] 
    public void TryExecute_SimpleLinkSystemLogicFunction_Null_Success() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      Assert.IsTrue(weapon.tryToExecute(WeaponStatsLinkSystemComponent.Type.IncreaseDamageBonus));
    }

    [TestMethod] 
    public void TryExecute_SimpleLinkSystemLogicFunction_Null_Failure() {
      Item item = Item.Types.Get("Items.Apple").Make();
      Assert.IsFalse(item.tryToExecute(WeaponStatsLinkSystemComponent.Type.IncreaseDamageBonus));
    }

    [TestMethod] 
    public void Execute_AdvancedLinkSystemLogicFunction_Null_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.execute(WeaponStatsLinkSystemComponent.Type.AdvancedIncreaseDamageBonus);
      Assert.AreEqual(
        2,
        (weapon.getComponent(WeaponStatsLinkSystemComponent.ModelData.Id) as WeaponStatsLinkSystemComponent.ModelData).damageBonus
      );
    }

    [TestMethod]
    public void Execute_AdvancedLinkSystemLogicFunction_Null_Failure() {
      Item item = Item.Types.Get("Items.Apple").Make();
      // TODO: this should throw a custom descriptive exception
      Assert.ThrowsException<KeyNotFoundException>(
        () => item.execute(WeaponStatsLinkSystemComponent.Type.AdvancedIncreaseDamageBonus)
      );
    }

    [TestMethod] 
    public void TryExecute_AdvancedLinkSystemLogicFunction_Null_Value() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      weapon.tryToExecute(WeaponStatsLinkSystemComponent.Type.AdvancedIncreaseDamageBonus);
      Assert.AreEqual(
        2,
        (weapon.getComponent(WeaponStatsLinkSystemComponent.ModelData.Id) as WeaponStatsLinkSystemComponent.ModelData).damageBonus
      );
    }

    [TestMethod] 
    public void TryExecute_AdvancedLinkSystemLogicFunction_Null_Success() {
      Weapon weapon = Item.Types.Get("Items.Weapons.Sword").Make<Weapon>();
      Assert.IsTrue(weapon.tryToExecute(WeaponStatsLinkSystemComponent.Type.AdvancedIncreaseDamageBonus));
    }

    [TestMethod] 
    public void TryExecute_AdvancedLinkSystemLogicFunction_Null_Failure() {
      Item item = Item.Types.Get("Items.Apple").Make();
      Assert.IsFalse(item.tryToExecute(WeaponStatsLinkSystemComponent.Type.AdvancedIncreaseDamageBonus));
    }
  }
}
