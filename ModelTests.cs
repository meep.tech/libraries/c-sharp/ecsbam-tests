using Meep.Tech.Data;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Examples;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Data.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.ECSBAM.Tests {

  [TestClass]
  public class ModelTests : BaseArchetypeDataTest {

    [TestClass]
    public class Colored_Component_No_Archetype {
      
      [TestMethod]
      public void Created_With_Default() {
        Item apple = (ColoredItem.Id.Archetype as Item.Type).Make();
        Assert.AreEqual(apple.getComponent<Color>().value, Color.DefaultColorValue);
      }
      
      [TestMethod]
      public void Created_With_Param() {
        const string ColorValue = "Blue";

        Item berry = Item.Types.Get<ColoredItem>().Make(ColorValue);
        Assert.AreEqual(berry.getComponent<Color>().value, ColorValue);
      }
    }

    [TestMethod]
    public void Equality() {
      Item original = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, "test"}
      });

      Item duplicate = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, "test"}
      });

      Assert.AreEqual(original, duplicate);
    }

    [TestMethod]
    public void Equality_Inheritance_Weapon() {
      Weapon original = Weapon.Types.Get(Sword.Id).Make<Weapon>(new Dictionary<Data.Param, object> {
        {Param.UniqueId, "test"}
      });

      Item duplicate = Item.Types.Get(Sword.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, "test"}
      });

      Assert.AreEqual(original, duplicate);
    }

    #region Serialization and Deserialization

    [TestMethod]
    public void Serialize_DataField() {
      string testId = "test";

      Item item = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      var serializedData = item.serialize(false);

      Assert.AreEqual(
        testId,
        serializedData.sqlData[Serializer.SQLIDColumnName]
      );
    }

    [TestMethod]
    public void Serialize_DataField_GetId() {
      string testId = "test";

      Item item = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      var serializedData = item.serialize(false);

      Assert.AreEqual(
        testId,
        serializedData.ModelUniqueId
      );
    }

    [TestMethod]
    public void Serialize_Component() {
      Item item = Item.Types.Get(Apple.Id).Make();

      var serializedData = item.serialize(false);

      JObject serializedComponentData 
        = JObject.Parse(
          serializedData.sqlData
            [Serializer.ComponentKeyNameAndTablePlaceholder]
            as string
        );

      Assert.AreEqual(
        // TODO: test toJobject as well in componenttests
        item.dataComponents[StackQuantity.TypeId].toJObject().ToString(),
        serializedComponentData.Value<JObject>(StackQuantity.TypeId.Archetype.ExternalId).ToString()
      );
    }

    [TestMethod]
    public void Deserialize_DataField_Single() {
      string testId = "test";

      Item item = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      var serializedData = item.serialize(false);

      Item deserializedItem = item.type.Make(serializedData);

      Assert.AreEqual(item.getUniqueId(), deserializedItem.getUniqueId());
    }

    [TestMethod]
    public void Deserialize_DataField_UniqueId_Equality() {
      string testId = "test";

      Item item = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      var serializedData = item.serialize(false);

      Item deserializedItem = item.type.Make(serializedData);

      Assert.IsTrue(IUnique.Equals(item, deserializedItem));
    }

    [TestMethod]
    public void Deserialize_Component_Single() {
      Item item = Item.Types.Get(Apple.Id).Make();

      var serializedData = item.serialize(false);

      Item deserializedItem = item.type.Make(serializedData);

      Assert.AreEqual(item.dataComponents[StackQuantity.TypeId], deserializedItem.dataComponents[StackQuantity.TypeId]);
    }

    [TestMethod]
    public void Deserialize_EntireModel() {
      string testId = "test";

      Item item = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      Serializer.SerializedData serializedData
        = item.serialize(false);
      
      Item deserializedItem 
        = item.type.Make(serializedData);

      Assert.AreEqual(item, deserializedItem);
    }

    [TestMethod]
    public void Deserialize_EntireModel_Weapon() {
      string testId = "test";

      Item item = Weapon.Types.Get(SpecialSword.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      var serializedData = item.serialize(false);
      
      Item deserializedItem = item.type.Make(serializedData);

      Assert.AreEqual(item, deserializedItem);
    }

    [TestMethod]
    public void Deserialize_EntireModel_FromJson_Static() {
      string testId = "test";

      Item item = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      string serializedData = item.toJson();
      
      Item deserializedItem 
        = Item.Types.Get(Apple.Id).Make(
          Serializer.SerializedData.FromJson(serializedData)
        );

      Assert.AreEqual(item, deserializedItem);
    }

    [TestMethod]
    public void Deserialize_EntireModel_FromJson_Instance() {
      string testId = "test";

      Item item = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      string serializedData = item.toJson();
      
      Item deserializedItem 
        = item.type.Make(Serializer.SerializedData.FromJson(serializedData));

      Assert.AreEqual(item, deserializedItem);
    }

    [TestMethod]
    public void Deserialize_EntireModel_FromJson_Collection() {
      string testId = "test";

      Item item = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      string serializedData = item.toJson();
      
      Item deserializedItem 
        = Item.Types.MakeFromJson(serializedData);

      Assert.AreEqual(item, deserializedItem);
    }

    [TestMethod]
    public void Deserialize_EntireModel_FromJson_Collection_Weapon() {
      string testId = "test";

      Weapon item = Weapon.Types.Get(Sword.Id).Make<Weapon>(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      string serializedData = item.toJson();
      
      Item deserializedItem 
        = Item.Types.MakeFromJson(serializedData);

      Assert.AreEqual(item, deserializedItem);
    }

    [TestMethod]
    public void Deserialize_Component_FullySerialized() {
      string testId = "test";

      Weapon item = Weapon.Types.Get(ThrowingStars.Id).Make<Weapon>(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      Serializer.SerializedData serializedData
        = item.serialize();

      Serializer.SerializedData serializedComponent 
        = serializedData.getChildren(Serializer.ComponentKeyNameAndTablePlaceholder).First();

      IModelDataComponent currentComponent = item.getComponent<WeaponWear>();
      IModelDataComponent deserializedComponent = (WeaponWear.Type.Id.Archetype as WeaponWear.Type).Make(serializedComponent);

      Assert.IsTrue(currentComponent.Equals(deserializedComponent));
    }
    

    [TestMethod]
    public void Deserialize_FullModelWithComponent_FullySerialized() {
      string testId = "test";

      Weapon item = Weapon.Types.Get(ThrowingStars.Id).Make<Weapon>(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      item.getComponent<WeaponWear>().wearLevel = 100;

      Serializer.SerializedData serializedData
        = item.serialize();

      Weapon newItem = Item.Types.Get(ThrowingStars.Id).Make<Weapon>(serializedData);

      Assert.AreEqual(item, newItem);
    }
    

    [TestMethod]
    public void Deserialize_FullModelWithComponent_ItemSerialized() {
      string testId = "test";

      Weapon item = Weapon.Types.Get(ThrowingStars.Id).Make<Weapon>(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      item.getComponent<WeaponWear>().wearLevel = 100;

      Serializer.SerializedData serializedData
        = item.serialize();

      Weapon newItem = Item.Types.Get(ThrowingStars.Id).Make<Weapon>(serializedData);

      Assert.AreEqual(
        item.getComponent<WeaponWear>().wearLevel,
        newItem.getComponent<WeaponWear>().wearLevel
      );
    }
    
    

    [TestMethod]
    public void Deserialize_FullModelWithComponent_FullySerialized_WrongDeserializationType() {
      string testId = "test";

      Weapon item = Weapon.Types.Get(ThrowingStars.Id).Make<Weapon>(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      item.getComponent<WeaponWear>().wearLevel = 100;

      Serializer.SerializedData serializedData
        = item.serialize();

      Assert.ThrowsException<System.InvalidCastException>(() =>
        Item.Types.Get(Apple.Id).Make(serializedData));
    }
    

    [TestMethod]
    public void Deserialize_FullModelWithComponent_FullySerialized_AfterBasicItem() {
      Deserialize_EntireModel();

      string testId = "test";

      Weapon item = Weapon.Types.Get(ThrowingStars.Id).Make<Weapon>(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      item.getComponent<WeaponWear>().wearLevel = 100;

      Serializer.SerializedData serializedData
        = item.serialize();

      Weapon newItem = Item.Types.Get(ThrowingStars.Id).Make<Weapon>(serializedData);

      Assert.AreEqual(item, newItem);
    }
    

    [TestMethod]
    public void Deserialize_FullModelWithComponent_FullySerialized_NotEqual() {
      string testId = "test";

      Weapon item = Weapon.Types.Get(ThrowingStars.Id).Make<Weapon>(new Dictionary<Data.Param, object> {
        {Param.UniqueId, testId}
      });

      item.getComponent<WeaponWear>().wearLevel = 100;

      Serializer.SerializedData serializedData
        = item.serialize();

      Weapon newItem = Item.Types.Get(ThrowingStars.Id).Make<Weapon>(serializedData);

      newItem.getComponent<WeaponWear>().wearLevel = 50;

      Assert.AreNotEqual(item, newItem);
    }


    #endregion

    [TestMethod]
    public void Copy_EqualsOriginal() {
      Item original = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, "test"}
      });

      Item copy = original.copy(false) as Item;
      Assert.AreEqual(original, copy);
    }

    [TestMethod]
    public void Clone_EqualsOriginal() {
      Item original = Item.Types.Get(Apple.Id).Make(new Dictionary<Data.Param, object> {
        {Param.UniqueId, "test"}
      });

      Item clone = original.clone(false);
      Assert.AreEqual(original, clone);
    }
  }
}
