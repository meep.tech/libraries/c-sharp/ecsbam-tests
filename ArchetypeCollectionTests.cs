using Meep.Tech.Data.Base;
using Meep.Tech.Data.Examples;
using Meep.Tech.Data.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Meep.Tech.ECSBAM.Tests {

  [TestClass]
  public class ArchetypeCollectionTests {

    [TestClass]
    public class GetTests : BaseArchetypeDataTest {

      #region Success

      #region Static Values

      [TestMethod]
      public void FromId() {
        Assert.AreEqual(
          Apple.Id,
          Item.Types.Get(Apple.Id).Id
        );
      }

      [TestMethod]
      public void FromInternalId_Static() {
        Assert.AreEqual(
          Apple.Id.InternalId,
          Item.Types.Get(Apple.Id.InternalId).InternalId
        );
      }

      [TestMethod]
      public void FromExternalId_Static() {
        Assert.AreEqual(
          Apple.Id.ExternalId,
          Item.Types.Get(Apple.Id.ExternalId).ExternalId
        );
      }

      [TestMethod]
      public void FromType_Static() {
        Assert.AreEqual(
          Apple.Id.ArchetypeType,
          Item.Types.Get(Apple.Id.ArchetypeType).GetType()
        );
      }

      /// <summary>
      /// TODO: this should be in archetype tests?
      /// </summary>
      [TestMethod]
      public void FromType_BasetypeEquality() {
        Assert.AreEqual(
          typeof(Item.Type),
          Item.Types.Get(typeof(Apple)).BaseArchetypeType
        );
      }

      /// <summary>
      /// Todo: this should be covered by equality tests in id tests?
      /// </summary>
      [TestMethod]
      public void FromExternalId_ValueEqualsStatic() {
        Item.Type appleArchetype = Item.Types.Get("Items.Apple");
        Assert.AreEqual(appleArchetype.Id, Apple.Id);
      }

      #endregion

      #region Constant Values

      [TestMethod]
      public void FromInternalId_Value() {
        Assert.AreEqual(
          11, Item.Types.Get(11).InternalId
        );
      }

      [TestMethod]
      public void FromExternalId_Value() {
        Assert.AreEqual(
          "Items.Apple",
          Item.Types.Get("Items.Apple").ExternalId
        );
      }

      [TestMethod]
      public void FromType_Value() {
        Assert.AreEqual(
          typeof(Apple),
          Item.Types.Get(typeof(Apple)).GetType()
        );
      }

      #endregion

      #endregion

      #region Failures

      #region Constant Values

      [TestMethod]
      public void FromInternalId_MissingValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeNotFoundException>(
          () => Item.Types.Get(100)
        );
      }

      [TestMethod]
      public void FromExternalId_MissingValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeNotFoundException>(
          () => Item.Types.Get("Items.DoesntExist")
        );
      }

      [TestMethod]
      public void FromExternalId_IncorrectArchetypeTypeValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeTypeMismatchException>(
          () => Item.Types.Get("Tiles.Grass")
        );
      }

      [TestMethod]
      public void FromType_IncorrectArchetypeTypeValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeTypeMismatchException>(
          () => Item.Types.Get(typeof(Grass))
        );
      }

      #endregion

      #endregion

    }

    [TestClass]
    public class GetITests : BaseArchetypeDataTest {

      #region Success

      #region Static Values

      [TestMethod]
      public void FromId() {
        Assert.AreEqual(
          Apple.Id,
          Item.Types.GetI(Apple.Id).Id
        );
      }

      [TestMethod]
      public void FromInternalId_Static() {
        Assert.AreEqual(
          Apple.Id.InternalId,
          Item.Types.GetI(Apple.Id.InternalId).InternalId
        );
      }

      [TestMethod]
      public void FromExternalId_Static() {
        Assert.AreEqual(
          Apple.Id.ExternalId,
          Item.Types.GetI(Apple.Id.ExternalId).ExternalId
        );
      }

      [TestMethod]
      public void FromType_Static() {
        Assert.AreEqual(
          Apple.Id.ArchetypeType,
          Item.Types.GetI(Apple.Id.ArchetypeType).GetType()
        );
      }

      [TestMethod]
      public void FromExternalId_ValueEqualsStatic() {
        IArchetype appleArchetype = Item.Types.GetI("Items.Apple");
        Assert.AreEqual(appleArchetype.Id, Apple.Id);
      }

      #endregion

      #region Constant Values

      [TestMethod]
      public void FromInternalId_Value() {
        Assert.AreEqual(
          11, Item.Types.GetI(11).InternalId
        );
      }

      [TestMethod]
      public void FromExternalId_Value() {
        Assert.AreEqual(
          "Items.Apple",
          Item.Types.GetI("Items.Apple").ExternalId
        );
      }

      [TestMethod]
      public void FromType_Value() {
        Assert.AreEqual(
          typeof(Apple),
          Item.Types.GetI(typeof(Apple)).GetType()
        );
      }

      #endregion

      #endregion

      #region Failures

      #region Constant Values

      [TestMethod]
      public void FromInternalId_MissingValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeNotFoundException>(
          () => Item.Types.GetI(100)
        );
      }

      [TestMethod]
      public void FromExternalId_MissingValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeNotFoundException>(
          () => Item.Types.GetI("Items.DoesntExist")
        );
      }

      [TestMethod]
      public void FromExternalId_IncorrectArchetypeTypeValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeTypeMismatchException>(
          () => Item.Types.GetI("Tiles.Grass")
        );
      }

      #endregion

      #region Static Values

      [TestMethod]
      public void FromId_IncorrectArchetypeTypeValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeTypeMismatchException>(
          () => Item.Types.GetI(Grass.Id)
        );
      }

      #endregion

      #endregion
    
    }
    
    [TestClass]
    public class GetGenericTests : BaseArchetypeDataTest {

      #region Success

      #region Static Values

      [TestMethod]
      public void FromId() {
        Assert.AreEqual(
          Apple.Id,
          Item.Types.Get<Item.Type>(Apple.Id).Id
        );
      }

      [TestMethod]
      public void FromInternalId_Static() {
        Assert.AreEqual(
          Apple.Id.InternalId,
          Item.Types.Get<Item.Type>(Apple.Id.InternalId).InternalId
        );
      }

      [TestMethod]
      public void FromExternalId_Static() {
        Assert.AreEqual(
          Apple.Id.ExternalId,
          Item.Types.Get<Item.Type>(Apple.Id.ExternalId).ExternalId
        );
      }

      [TestMethod]
      public void FromType_Static() {
        Assert.AreEqual(
          Apple.Id.ArchetypeType,
          Item.Types.Get<Item.Type>(Apple.Id.ArchetypeType).GetType()
        );
      }

      /// <summary>
      /// TODO: this should be in archetype tests?
      /// </summary>
      [TestMethod]
      public void FromType_BasetypeEquality() {
        Assert.AreEqual(
          typeof(Item.Type),
          Item.Types.Get<Item.Type>(typeof(Apple)).BaseArchetypeType
        );
      }

      /// <summary>
      /// Todo: this should be covered by equality tests in id tests?
      /// </summary>
      [TestMethod]
      public void FromExternalId_ValueEqualsStatic() {
        Item.Type appleArchetype = Item.Types.Get<Item.Type>("Items.Apple");
        Assert.AreEqual(appleArchetype.Id, Apple.Id);
      }

      #endregion

      #region Constant Values

      [TestMethod]
      public void FromInternalId_Value() {
        Assert.AreEqual(
          11, Item.Types.Get<Item.Type>(11).InternalId
        );
      }

      [TestMethod]
      public void FromExternalId_Value() {
        Assert.AreEqual(
          "Tiles.Grass",
          Tile.Types.Get<Tile.Type>("Tiles.Grass").ExternalId
        );
      }

      [TestMethod]
      public void FromType_Value() {
        Assert.AreEqual(
          typeof(Apple),
          Item.Types.Get<Item.Type>(typeof(Apple)).GetType()
        );
      }

      #endregion

      #endregion

      #region Failures

      #region Constant Values

      [TestMethod]
      public void FromInternalId_MissingValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeNotFoundException>(
          () => Item.Types.Get<Item.Type>(100)
        );
      }

      [TestMethod]
      public void FromExternalId_MissingValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeNotFoundException>(
          () => Item.Types.Get<Item.Type>("Items.DoesntExist")
        );
      }

      [TestMethod]
      public void FromExternalId_IncorrectArchetypeTypeValue() {
        Assert.ThrowsException<Archetype.Collection.ArchetypeTypeMismatchException>(
          () => Item.Types.Get<Item.Type>("Tiles.Grass")
        );
      }

      #endregion

      #endregion

    }
  }
}
