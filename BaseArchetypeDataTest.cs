﻿using Meep.Tech.Data.Loading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Meep.Tech.ECSBAM.Tests {

  [TestClass]
  public abstract class BaseArchetypeDataTest {

    [AssemblyInitialize]
    public static void Setup(TestContext testContext) {

      // makes sure the assembly for examples is loaded already:
      var initalizer = Meep.Tech.Data.Examples.Apple.Id;

      // read all the types:
      if (!ArchetypeLoader.IsSealed) {
        ArchetypeLoader.Settings.FatalOnCannotInitializeArchetype = true;
        ArchetypeLoader.TryToLoadAllArchetypes();
      }
    }
  }
}
